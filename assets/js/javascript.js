/* ==============================   ABOUT SET AUTOMATIC SLIDE   ============================== */

var myIndex = 0;
carousel();

function carousel() {
  var i;
  var x = document.getElementsByClassName("mySlides");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  myIndex++;
  if (myIndex > x.length) {myIndex = 1}
  x[myIndex-1].style.display = "block";
  setTimeout(carousel, 12000);
}

/* ==============================   ABOUT SET SLIDE   ============================== */

  var slideIndex = 0;
  showDivs(slideIndex);

  function setSlide(n) {
    showDivs(n);
  }

  function showDivs(n) {
    var i;
    var x = document.getElementsByClassName("mySlides");
    for (i = 0; i < x.length; i++) {
      x[i].style.display = "none";
    }
    x[n].style.display = "block";
  }


  /* ==============================   PROJECT TABS   ============================== */

  var current = 1;
  function openService(serviceName, ID) {
    var filter = document.getElementById("filter");
    if(ID == 1) {
      var elem = document.getElementsByClassName("service-links");
      elem[0].className = elem[0].className.replace(" active" + current , "");
      current = 1;
      elem[0].className += " active" + current;
    } else if (ID == 2) {
      var elem = document.getElementsByClassName("service-links");
      elem[0].className = elem[0].className.replace(" active" + current , "");
      current = 2;
      elem[0].className += " active" + current;
    } else {
      var elem = document.getElementsByClassName("service-links");
      elem[0].className = elem[0].className.replace(" active" + current , "");
      current = 3;
      elem[0].className += " active" + current;
    }

    var i;
    var x = document.getElementsByClassName("service");
    for (i = 0; i < x.length; i++) {
      x[i].style.display = "none";
    }
    document.getElementById(serviceName).style.display = "flex";
  }

  /* ==============================   NAV COLOR ON SCROLL   ============================== */

  window.onscroll = function() {scrollFunction(), scrollBorder()};


function scrollFunction() {
  if (document.body.scrollTop > 70 || document.documentElement.scrollTop > 70) {
    document.getElementById("header").style.background = "rgba(50, 50, 50, .9)";
  } else {
    document.getElementById("header").style.background = "rgba(50, 50, 50, .25)";
  }
}



/* ==============================   SERVICES   ============================== */

function showServiceText(n) {
  var i;
  var x = document.getElementsByClassName("service-text");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  x[n].style.display = "block";
}

/* ==============================   SHOW MORE/LESS   ============================== */
function showMoreLess() {
  var x = document.getElementsByClassName("flex-item");
  var state = document.getElementById("more-btn");
  if(state.value == "Prikaži više") {
      for(i = 2; i < x.length; i++) {
        x[i].style.display = "flex";
      }

      state.value = "Prikaži manje";
  } else {
      for(i = x.length-1; i < x.length; i++) {
        x[i].style.display = "none";
      }
      state.value = "Prikaži više";
  }
}

/* ==============================   OVERLAY   ============================== */



function overlay(n) {
  var name = "service-overlay" + n;
  var item = document.getElementById(name)

  item.style.display = "block";
}

function back(n) {
  var name = "service-overlay" + n;
  var item = document.getElementById(name)

  item.style.display = "none";
}


/* ==============================   HAMBURGER   ============================== */

function hamburger() {
  var x = document.getElementById("myDIV");
  if (x.style.display === "block") {
    x.style.display = "none";
  } else {
    x.style.display = "block";
  }
}


/* ==============================   API   ============================== */

function setApiUrl(apiUrl) {
  localStorage.setItem("apiUrl", apiUrl);
}

function getApiUrl() {
  return localStorage.getItem("apiUrl");
}


/* ==============================   PROJECTS   ============================== */
var projects = [];

function getProjectsFromApi(){
  var apiUrl = getApiUrl();
  var request = new XMLHttpRequest();
  request.open('GET', apiUrl + '/api/projects', true);
  request.onload = function() {
    var data = JSON.parse(this.response);
    if (request.status >= 200 && request.status < 400) {
      projects = data;
      setProjects(projects);
    } else {
      console.log('error');
    }
  }
  request.send();
}


function setProjects(projects) {
  var projectsContainer = document.getElementById("web");
  projectsContainer.innerHTML = "";
  projects["projects"].forEach(function(project) {
    var projectTemplate = `
      <div class="flex-item">
        <img src="${project.image_url}" alt="${project.title}">
        <div class="flex-item-content">
          <h5>${project.category}</h5>
          <h2>${project.title}</h2>
          <p><i>"${project.description}"</i></p>
          <div class="languages">
            ${project.technologies}
          </div>
          <div class="idina">
            <a href="${project.website}" target="_blank">Idi na web stranicu</a>
          </div>
        </div>
      </div>
    `;
    projectsContainer.innerHTML += projectTemplate;
  });
}

/* ==============================   NEWSLETTER   ============================== */

var newsletterInput = document.querySelector('input[name="newsletter"]');
var newsletterData = JSON.parse(newsletterInput.dataset.newsletter);
newsletterData.email = newsletterInput.value;
newsletterInput.dataset.newsletter = JSON.stringify(newsletterData);

document.getElementById("subscribe").addEventListener("click", function() {
  var newsletterInput = document.querySelector('input[name="newsletter"]');
  var newsletterData = JSON.parse(newsletterInput.dataset.newsletter);
  newsletterData.email = newsletterInput.value;
  newsletterInput.dataset.newsletter = JSON.stringify(newsletterData);
  var request = new XMLHttpRequest();
  request.open('POST', getApiUrl() + '/api/newsletter', true);
  request.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
  request.onload = function() {
    var data = JSON.parse(this.response);
    if (request.status === 422) {
      alert(data.errors);
    } else if (request.status >= 200 && request.status < 400) {
      alert(data.message);
    } else {
      alert(data.message);
    }
  }
  request.send(JSON.stringify(newsletterData));
});


getProjectsFromApi();


